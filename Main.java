package com;


/*
    1. Modeling (Class, Abstract Class, Interface, Enum, Exception, Collections, ArrayList, Encapsulation, Abstraction, Composition, Polymorphism)
    Create a program that will help manage a library. So far we will store
    - books (they have title, author(s), genre (eg like Fantasy, Self-Help, Science), and a unique identifier: ISBN (International Standard Book Number))
    - scientific articles (they also have title, author(s), a theme (eg: Computer Science, Biology, Psychology) and a unique identifier DOI)
    - movies ( they have title and genre, a list of participating actors, and they can be identified via an IMDB id)

    2. Read from file
    We would need to read books, articles and movies from a file and store them.

    3. Search through data
    We would like to be able to query
        - books by their title
        - by one of the authors
 */

import publication.Movie;
import publication.enums.Genre;
import publication.exceptions.InvalidMovieIdentifier;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        Movie matrix = null;
        try {
            matrix = new Movie("Matrix", Genre.FANTASY, "12%35");
        } catch (InvalidMovieIdentifier e) {
            System.out.println("Exception caught, program continues");
            System.out.println(e.getMessage());
        }
        System.out.println(matrix.getTitle());
        System.out.println("Program finished successfully");
    }

}
